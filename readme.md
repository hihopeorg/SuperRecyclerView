# SuperRecyclerView

**本项目是基于开源项目 SuperRecyclerView 进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/Malinskiy/SuperRecyclerView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：SuperRecyclerView
- 所属系列：ohos的第三方组件适配移植
- 功能：集成上拉加载下拉刷新侧滑删除的ListContainer
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 调用差异：无
- 原项目Doc地址：https://github.com/Malinskiy/SuperRecyclerView
- 原基线版本：v1.1.4
- 编程语言：Java 

#### 安装教程

1. 下载本三方库的har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在entry级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.malinskiy.ohos:superrecyclerview:1.0.0'
}
```
#### 效果演示
<img src="gif/演示效果.gif"/>
#### 使用说明

1.在布局xml中导入封装好的ListContainer控件SuperRecyclerView，设置相应宽高属性

```
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">
    <com.malinskiy.superrecyclerview.SuperRecyclerView
        ohos:id="$+id:list_superrecyclerview"
        ohos:height="match_parent"
        ohos:width="match_parent"/>
</DirectionalLayout>
```
2.设置下拉刷新功能
实现RefreshListener接口方法并将接口设置给SuperRecyclerView
```
    @Override
    public void onRefresh() {
        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                Toast.show(getContext(),"Refresh");
                mAdapter.add("New stuff");
                mRecycler.refreshFinish();
            }
        },2000);
    }

    @Override
    public boolean enableRefresh() {
        return true;
    }
	
	mRecycler.setRefreshListener(this);
```
注意在onRefresh方法中最后一定要回调SuperRecyclerView的refreshFinish方法
3.设置上拉加载功能
实现OnMoreListener接口方法并将接口设置给SuperRecyclerView
```
    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {

        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                Toast.show(getContext(),"More");
                mAdapter.add("More asked, more served");
            }
        },300);
    }
	
	 mRecycler.setupMoreListener(this,1);
```
4.设置侧滑删除功能
使用BaseSwipeAdapter适配器并将其设置给SuperRecyclerView
```
    List<String> placeHolderList = new ArrayList<>();
    for (int i = 0; i < 6; i++) {
        placeHolderList.add("More stuff");
    }
    mAdapter = new BaseSwipeAdapter(getContext(),placeHolderList);
    mRecycler = (SuperRecyclerView) findComponentById(ResourceTable.Id_list_superrecyclerview);
    mRecycler.setAdapter(mAdapter);
```

#### 版本迭代

- v1.0.0

    -支持上拉加载更多
	
	-支持下拉刷新
	
	-支持侧滑子项显示删除功能
	
	暂不支持手势侧滑直接删除子项

#### 版权和许可信息
```
Copyright (c) 2016 Anton Malinskiy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
Come on, don't tell me you read that.
```


