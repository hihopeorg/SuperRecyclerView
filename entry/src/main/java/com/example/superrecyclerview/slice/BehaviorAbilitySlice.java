package com.example.superrecyclerview.slice;

import com.example.superrecyclerview.ResourceTable;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.refresh.SwipeRefreshLayout;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.LayoutManager;
import ohos.agp.components.ListContainer;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.ArrayList;
import java.util.List;

public class BehaviorAbilitySlice extends AbilitySlice implements SwipeRefreshLayout.RefreshListener, OnMoreListener{

    private SuperRecyclerView mRecycler;
    private StringListAdapter mAdapter;
    private LayoutManager mlayoutManager;
    private EventHandler mHandler;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_superrecyclerview_behavior);

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        List<StringListAdapter.ExampleData> placeHolderList = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            placeHolderList.add(new StringListAdapter.ExampleData("More stuff"));
        }
        mAdapter = new StringListAdapter(placeHolderList,getContext());
        mRecycler = (SuperRecyclerView) findComponentById(ResourceTable.Id_list_superrecyclerview);
        mRecycler.setAdapter(mAdapter);
        mlayoutManager = getLayoutManager();
        mRecycler.setLayoutManager(mlayoutManager);
        mRecycler.setRefreshListener(this);
        mRecycler.setupMoreListener(this,1);

    }


    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {

        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                ListAbilitySlice.Toast.show(getContext(),"More");
                mAdapter.add("More asked, more served");
            }
        },300);
    }

    @Override
    public void onRefresh() {

        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                ListAbilitySlice.Toast.show(getContext(),"Refresh");
                mAdapter.add("New stuff");
                mRecycler.refreshFinish();
            }
        },2000);
    }

    @Override
    public boolean enableRefresh() {
        return true;
    }


    protected boolean isSwipeToDismissEnabled() {
        return true;
    }


    protected LayoutManager getLayoutManager(){
        DirectionalLayoutManager directionalLayoutManager =  new DirectionalLayoutManager();
        directionalLayoutManager.setOrientation(Component.VERTICAL);
        return directionalLayoutManager;
    }
}
