package com.example.superrecyclerview.slice;

import com.example.superrecyclerview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button button1 = (Button) findComponentById(ResourceTable.Id_button_list_sample);
        button1.setClickedListener(component -> present(new ListAbilitySlice(), new Intent()));

        Button button2 = (Button) findComponentById(ResourceTable.Id_button_grid_sample);
        button2.setClickedListener(component -> present(new GridAbilitySlice(), new Intent()));

        Button button3 = (Button) findComponentById(ResourceTable.Id_button_staggered_sample);
        button3.setClickedListener(component -> present(new StaggeredGridAbilitySlice(), new Intent()));

        Button button4 = (Button) findComponentById(ResourceTable.Id_button_swipe_sample);
        button4.setClickedListener(component -> present(new SwipeAbilitySlice(), new Intent()));

        Button button5 = (Button) findComponentById(ResourceTable.Id_button_header_sample);
        button5.setClickedListener(component -> present(new HeaderAbilitySlice(), new Intent()));

        Button button6 = (Button) findComponentById(ResourceTable.Id_button_behavior);
        button6.setClickedListener(component -> present(new BehaviorAbilitySlice(), new Intent()));

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
