package com.example.superrecyclerview.slice;

import com.malinskiy.superrecyclerview.swipe.BaseSwipeAdapter;
import ohos.aafwk.ability.AbilitySlice;

import com.example.superrecyclerview.ResourceTable;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.refresh.SwipeRefreshLayout;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.LayoutManager;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.ArrayList;
import java.util.List;

public class SwipeAbilitySlice extends AbilitySlice implements SwipeRefreshLayout.RefreshListener, OnMoreListener{

    private SuperRecyclerView mRecycler;
    private BaseSwipeAdapter mAdapter;
    private LayoutManager mlayoutManager;
    private EventHandler mHandler;
    private int i = 0;
    private int j = 0;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_superrecyclerview_swipe);

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        List<String> placeHolderList = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            placeHolderList.add("More stuff"+i);
        }
        mAdapter = new BaseSwipeAdapter(getContext(),placeHolderList);
        mRecycler = (SuperRecyclerView) findComponentById(ResourceTable.Id_list_superrecyclerview);
        mRecycler.setAdapter(mAdapter);
        mlayoutManager = getLayoutManager();
        mRecycler.setLayoutManager(mlayoutManager);
        mRecycler.setRefreshListener(this);
        mRecycler.setupMoreListener(this,1);



    }


    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {

        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                Toast.show(getContext(),"More");
                mAdapter.add("More asked, more served"+i);
                i++;
            }
        },300);
    }

    @Override
    public void onRefresh() {

        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                Toast.show(getContext(),"Refresh");
                mAdapter.add("New stuff"+j);
                mRecycler.refreshFinish();
                j++;
            }
        },2000);
    }

    @Override
    public boolean enableRefresh() {
        return true;
    }



    protected boolean isSwipeToDismissEnabled() {
        return true;
    }


    protected LayoutManager getLayoutManager(){
        DirectionalLayoutManager directionalLayoutManager =  new DirectionalLayoutManager();
        directionalLayoutManager.setOrientation(Component.VERTICAL);
        return directionalLayoutManager;
    }


    public static class Toast {
        public static void show(Context context, String content) {
            DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_layout_toast, null, false);
            Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_toast);
            text.setText(content);
            new ToastDialog(context)
                    .setComponent(toastLayout)
                    .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                    .setAlignment(LayoutAlignment.CENTER)
                    .setDuration(1000)
                    .show();
        }
    }

}

