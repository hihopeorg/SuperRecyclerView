package com.example.superrecyclerview.slice;

import com.example.superrecyclerview.ResourceTable;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.List;

public class StringGridListAdapter extends BaseItemProvider {

    private List<ExampleData> placeholderList;

    private LayoutScatter inflater;

    private int itemWidth;

    public StringGridListAdapter(List<ExampleData> placeHolderList, Context context) {
        this.placeholderList = placeHolderList;
        this.inflater = LayoutScatter.getInstance(context);
    }

    public void add(String text){
        placeholderList.add(new ExampleData(text));
        notifyDataChanged();
    }

    public void remove(int position) {
        placeholderList.remove(position);
        notifyDataChanged();
    }

    @Override
    public int getCount() {
        return placeholderList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component vi = inflater.parse(ResourceTable.Layout_view_placeholder_grid, null, false);
        Text text = (Text) vi.findComponentById(ResourceTable.Id_text123);
        text.setText(placeholderList.get(i).getText());
        return vi;
    }


    public static class ExampleData {
        private String text;

        public ExampleData(String text) {
            setText(text);
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }

}
