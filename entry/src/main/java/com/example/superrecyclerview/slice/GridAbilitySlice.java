package com.example.superrecyclerview.slice;

import com.example.superrecyclerview.ResourceTable;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.malinskiy.superrecyclerview.refresh.SwipeRefreshLayout;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.LayoutManager;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.ArrayList;
import java.util.List;

public class GridAbilitySlice extends AbilitySlice implements SwipeRefreshLayout.RefreshListener, OnMoreListener{
    private SuperRecyclerView mRecycler;
    private StringGridListAdapter mAdapter;
    private LayoutManager mlayoutManager;
    private EventHandler mHandler;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_superrecyclerview_grid);

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        List<StringGridListAdapter.ExampleData> placeHolderList = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            placeHolderList.add(new StringGridListAdapter.ExampleData("More stuff"));
        }
        mAdapter = new StringGridListAdapter(placeHolderList,getContext());
        mRecycler = (SuperRecyclerView) findComponentById(ResourceTable.Id_list_superrecyclerview);

        mlayoutManager = getLayoutManager();
        mRecycler.setLayoutManager(mlayoutManager);
        mRecycler.setRefreshListener(this);
        mRecycler.setupMoreListener(this,1);
        boolean dismissEnabled = isSwipeToDismissEnabled();


        mRecycler.setAdapter(mAdapter);
    }


    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {

        mHandler.postTask(new Runnable() {

            @Override
            public void run() {
                ListAbilitySlice.Toast.show(getContext(),"More");
                mAdapter.add("More asked, more served");
            }
        },300);
    }

    @Override
    public void onRefresh() {

        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                ListAbilitySlice.Toast.show(getContext(),"Refresh");
                mAdapter.add("New stuff");
                mRecycler.refreshFinish();
            }
        },2000);
    }

    @Override
    public boolean enableRefresh() {
        return true;
    }



    protected boolean isSwipeToDismissEnabled() {
        return true;
    }


    protected LayoutManager getLayoutManager(){
        TableLayoutManager tableLayoutManager =  new TableLayoutManager();
        tableLayoutManager.setColumnCount(3);
        return tableLayoutManager;
    }
}
