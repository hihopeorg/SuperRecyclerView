package com.malinskiy.superrecyclerview.util.asutil;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class VelocityTrackerTest {

    @Test
    public void clear() {
        VelocityTracker velocityTracker = new VelocityTracker();
        velocityTracker.clear();
        try {
            Field field = VelocityTracker.class.getDeclaredField("needScale");
            field.setAccessible(true);
            Object fieldValue = field.get(velocityTracker);
            assertEquals(false,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void computeCurrentVelocity() {
        VelocityTracker velocityTracker = new VelocityTracker();
        velocityTracker.computeCurrentVelocity(789,546);
        try {
            Field field = VelocityTracker.class.getDeclaredField("needScale");
            field.setAccessible(true);
            Object fieldValue = field.get(velocityTracker);
            assertEquals(true,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void calculateCurrentVelocity() {
        VelocityTracker velocityTracker = new VelocityTracker();
        velocityTracker.calculateCurrentVelocity(789,546);
        try {
            Field field = VelocityTracker.class.getDeclaredField("needScale");
            field.setAccessible(true);
            Object fieldValue = field.get(velocityTracker);
            assertEquals(true,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCalculateCurrentVelocity() {
        VelocityTracker velocityTracker = new VelocityTracker();
        velocityTracker.calculateCurrentVelocity(789);
        try {
            Field field = VelocityTracker.class.getDeclaredField("needScale");
            field.setAccessible(true);
            Object fieldValue = field.get(velocityTracker);
            assertEquals(false,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getXVelocity() {
        VelocityTracker velocityTracker = new VelocityTracker();
        assertEquals(0,velocityTracker.getXVelocity(789),0.0);

    }


    @Test
    public void getYVelocity() {
        VelocityTracker velocityTracker = new VelocityTracker();
        assertEquals(0,velocityTracker.getYVelocity(789),0.0);
    }



}