package com.malinskiy.superrecyclerview.util.asutil;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class ViewDragHelperTest {
    public static Context context= AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    @Test
    public void setMinVelocity() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        viewDragHelper.setMinVelocity(1);
        assertEquals(1,viewDragHelper.getMinVelocity(),0.0);
    }

    @Test
    public void getMinVelocity() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        viewDragHelper.setMinVelocity(1);
        assertEquals(1,viewDragHelper.getMinVelocity(),0.0);
    }

    @Test
    public void getViewDragState() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        viewDragHelper.setDragState(1);
        assertEquals(1,viewDragHelper.getViewDragState(),0.0);
    }

    @Test
    public void setEdgeTrackingEnabled() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        viewDragHelper.setEdgeTrackingEnabled(1);
        try {
            Field field = ViewDragHelper.class.getDeclaredField("mTrackingEdges");
            field.setAccessible(true);
            Object fieldValue = field.get(viewDragHelper);
            assertEquals(1, (int) fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void getEdgeSize() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(60,viewDragHelper.getEdgeSize());
    }

    @Test
    public void getCapturedView() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(null,viewDragHelper.getCapturedView());
    }

    @Test
    public void getActivePointerId() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(-1,viewDragHelper.getActivePointerId());
    }

    @Test
    public void getTouchSlop() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(8,viewDragHelper.getTouchSlop());
    }

    @Test
    public void continueSettling() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(false,viewDragHelper.continueSettling(true));
    }

    @Test
    public void isPointerDown() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(false,viewDragHelper.isPointerDown(8));
    }


    @Test
    public void checkTouchSlop() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        try {
            Field field = ViewDragHelper.class.getDeclaredField("mInitialMotionX");
            field.setAccessible(true);
            field.set(viewDragHelper,new float[]{1,2});
            assertEquals(false,viewDragHelper.checkTouchSlop(8));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void isEdgeTouched() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        try {
            Field field = ViewDragHelper.class.getDeclaredField("mInitialEdgesTouched");
            field.setAccessible(true);
            field.set(viewDragHelper,new int[]{1,2});
            assertEquals(false,viewDragHelper.isEdgeTouched(8));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void isCapturedViewUnder() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(false,viewDragHelper.isCapturedViewUnder(8,9));
    }

    @Test
    public void isViewUnder() {
        ViewDragHelper viewDragHelper = ViewDragHelper.create(new ListContainer(context), new ViewDragHelper.Callback() {
            @Override
            public boolean tryCaptureView(Component child, int pointerId) {
                return false;
            }
        });
        assertEquals(false,viewDragHelper.isViewUnder(new Component(context),8,9));
    }
}