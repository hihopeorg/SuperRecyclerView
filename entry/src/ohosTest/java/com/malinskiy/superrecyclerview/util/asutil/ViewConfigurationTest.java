package com.malinskiy.superrecyclerview.util.asutil;

import org.junit.Test;

import static com.malinskiy.superrecyclerview.util.asutil.ViewDragHelperTest.context;
import static org.junit.Assert.*;

public class ViewConfigurationTest {

    @Test
    public void get() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(true,viewConfiguration instanceof ViewConfiguration);
    }

    @Test
    public void getLongPressTimeout() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(500,viewConfiguration.getLongPressTimeout());
    }

    @Test
    public void getTapTimeout() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(100,viewConfiguration.getTapTimeout());
    }

    @Test
    public void getDoubleTapTimeout() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(300,viewConfiguration.getDoubleTapTimeout());
    }

    @Test
    public void getDoubleTapMinTime() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(40,viewConfiguration.getDoubleTapMinTime());
    }

    @Test
    public void getTouchSlop() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(8,viewConfiguration.getTouchSlop());
    }

    @Test
    public void getScaledTouchSlop() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(8,viewConfiguration.getScaledTouchSlop());
    }

    @Test
    public void getDoubleTapSlop() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(100,viewConfiguration.getDoubleTapSlop());
    }

    @Test
    public void getScaledDoubleTapSlop() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(450,viewConfiguration.getScaledDoubleTapSlop());
    }

    @Test
    public void getMinimumFlingVelocity() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(50,viewConfiguration.getMinimumFlingVelocity());
    }

    @Test
    public void getScaledMinimumFlingVelocity() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(50,viewConfiguration.getScaledMinimumFlingVelocity());
    }

    @Test
    public void getMaximumFlingVelocity() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(8000,viewConfiguration.getMaximumFlingVelocity());
    }

    @Test
    public void getScaledMaximumFlingVelocity() {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        assertEquals(8000,viewConfiguration.getScaledMaximumFlingVelocity());
    }
}