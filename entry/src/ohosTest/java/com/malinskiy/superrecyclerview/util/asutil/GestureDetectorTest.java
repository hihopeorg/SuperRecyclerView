package com.malinskiy.superrecyclerview.util.asutil;

import ohos.eventhandler.EventHandler;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.Test;

import java.lang.reflect.Field;

import static com.malinskiy.superrecyclerview.util.asutil.ViewDragHelperTest.context;
import static org.junit.Assert.*;

public class GestureDetectorTest {

    @Test
    public void setOnDoubleTapListener() {
        GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(TouchEvent e) {
                return false;
            }

            @Override
            public void onShowPress(TouchEvent e) {

            }

            @Override
            public boolean onSingleTapUp(TouchEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(TouchEvent e) {

            }

            @Override
            public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        GestureDetector.OnDoubleTapListener onDoubleTapListener = new GestureDetector.SimpleOnGestureListener();
        gestureDetector.setOnDoubleTapListener(onDoubleTapListener);
        try {
            Field field = GestureDetector.class.getDeclaredField("mDoubleTapListener");
            field.setAccessible(true);
            Object fieldValue = field.get(gestureDetector);
            assertEquals(onDoubleTapListener,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setContextClickListener() {
        GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(TouchEvent e) {
                return false;
            }

            @Override
            public void onShowPress(TouchEvent e) {

            }

            @Override
            public boolean onSingleTapUp(TouchEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(TouchEvent e) {

            }

            @Override
            public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        GestureDetector.OnContextClickListener onDoubleTapListener = new GestureDetector.OnContextClickListener() {
            @Override
            public boolean onContextClick(TouchEvent e) {
                return false;
            }
        };
        gestureDetector.setContextClickListener(onDoubleTapListener);
        try {
            Field field = GestureDetector.class.getDeclaredField("mContextClickListener");
            field.setAccessible(true);
            Object fieldValue = field.get(gestureDetector);
            assertEquals(onDoubleTapListener,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setIsLongpressEnabled() {
        GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(TouchEvent e) {
                return false;
            }

            @Override
            public void onShowPress(TouchEvent e) {

            }

            @Override
            public boolean onSingleTapUp(TouchEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(TouchEvent e) {

            }

            @Override
            public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        gestureDetector.setIsLongpressEnabled(true);
        try {
            Field field = GestureDetector.class.getDeclaredField("mIsLongpressEnabled");
            field.setAccessible(true);
            Object fieldValue = field.get(gestureDetector);
            assertEquals(true,fieldValue);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void isLongpressEnabled() {
        GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(TouchEvent e) {
                return false;
            }

            @Override
            public void onShowPress(TouchEvent e) {

            }

            @Override
            public boolean onSingleTapUp(TouchEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(TouchEvent e) {

            }

            @Override
            public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        assertEquals(true,gestureDetector.isLongpressEnabled());

    }
}