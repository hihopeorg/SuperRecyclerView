package com.malinskiy.superrecyclerview.swipe;

import org.junit.Test;

import java.util.ArrayList;

import static com.malinskiy.superrecyclerview.util.asutil.ViewDragHelperTest.context;
import static org.junit.Assert.*;

public class BaseSwipeAdapterTest {

    @Test
    public void getCount() {
        BaseSwipeAdapter baseSwipeAdapter = new BaseSwipeAdapter(context,new ArrayList<>());
        assertEquals(0,baseSwipeAdapter.getCount());
    }


    @Test
    public void getItemId() {
        BaseSwipeAdapter baseSwipeAdapter = new BaseSwipeAdapter(context,new ArrayList<>());
        assertEquals(0,baseSwipeAdapter.getItemId(0));
    }

    @Test
    public void add() {
        BaseSwipeAdapter baseSwipeAdapter = new BaseSwipeAdapter(context,new ArrayList<>());
        baseSwipeAdapter.add("asad");
        assertEquals(1,baseSwipeAdapter.getCount());
    }
}