package com.example.superrecyclerview;

import com.example.superrecyclerview.slice.ListAbilitySlice;
import com.example.superrecyclerview.slice.MainAbilitySlice;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleTest {
    private static Ability ability = EventHelper.startAbility(MainAbility.class);

    @Test
    public void refreshTest() {
        stopThread(5000);
        AbilitySlice mainSlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        Button button1 = (Button) mainSlice.findComponentById(ResourceTable.Id_button_list_sample);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainSlice.getAbility(), button1);
        stopThread(5000);
        AbilitySlice listSlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        SuperRecyclerView mRecycler = (SuperRecyclerView)listSlice.findComponentById(ResourceTable.Id_list_superrecyclerview);
        ListContainer listContainer = (ListContainer) ((ComponentContainer)((ComponentContainer)mRecycler.getComponentAt(0)).getComponentAt(0)).getComponentAt(1);
        int listCount = listContainer.getChildCount();
        EventHelper.inputSwipe(ability, 500, 500, 500, 1200, 3000);
        stopThread(8000);
        int listCount1 = listContainer.getChildCount();
        listSlice.present(new MainAbilitySlice(), new Intent());
        assertEquals(false,listCount == listCount1);

    }

    @Test
    public void loadMoreTest() {
        stopThread(5000);
        AbilitySlice mainSlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        Button button1 = (Button) mainSlice.findComponentById(ResourceTable.Id_button_list_sample);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainSlice.getAbility(), button1);
        stopThread(5000);
        AbilitySlice listSlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        SuperRecyclerView mRecycler = (SuperRecyclerView)listSlice.findComponentById(ResourceTable.Id_list_superrecyclerview);
        ListContainer listContainer = (ListContainer) ((ComponentContainer)((ComponentContainer)mRecycler.getComponentAt(0)).getComponentAt(0)).getComponentAt(1);
        int listCount = listContainer.getChildCount();
        EventHelper.inputSwipe(ability, 500, 1200, 500, 500, 3000);
        stopThread(8000);
        int listCount1 = listContainer.getChildCount();
        listSlice.present(new MainAbilitySlice(), new Intent());
        assertEquals(false,listCount == listCount1);

    }

    @Test
    public void deleteTest() {
        stopThread(5000);
        AbilitySlice mainSlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        Button button1 = (Button) mainSlice.findComponentById(ResourceTable.Id_button_swipe_sample);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainSlice.getAbility(), button1);
        stopThread(5000);
        AbilitySlice listSlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        SuperRecyclerView mRecycler = (SuperRecyclerView)listSlice.findComponentById(ResourceTable.Id_list_superrecyclerview);
        ListContainer listContainer = (ListContainer) ((ComponentContainer)((ComponentContainer)mRecycler.getComponentAt(0)).getComponentAt(0)).getComponentAt(1);
        int listCount = listContainer.getChildCount();
        EventHelper.inputSwipe(ability, 1000, 1200, 0, 1200, 3000);
        stopThread(3000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(listSlice.getAbility(), listContainer.getComponentAt(2));
        stopThread(3000);
        int listCount1 = listContainer.getChildCount();
        listSlice.present(new MainAbilitySlice(), new Intent());
        assertEquals(true,listCount == listCount1);

    }

    private void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
