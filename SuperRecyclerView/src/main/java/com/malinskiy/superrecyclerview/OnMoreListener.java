package com.malinskiy.superrecyclerview;

public interface OnMoreListener {

    void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition);
}
