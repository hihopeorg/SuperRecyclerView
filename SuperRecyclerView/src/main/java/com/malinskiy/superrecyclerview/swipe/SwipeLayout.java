

package com.malinskiy.superrecyclerview.swipe;

import com.malinskiy.superrecyclerview.util.asutil.GestureDetector;
import com.malinskiy.superrecyclerview.util.asutil.ResourceUtil;
import com.malinskiy.superrecyclerview.util.asutil.ViewDragHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.multimodalinput.event.TouchEvent;


public class SwipeLayout extends ComponentContainer implements Component.TouchEventListener,
        Component.LayoutRefreshedListener, Component.EstimateSizeListener, ComponentContainer.ArrangeListener {
    // These states are used only for ViewBindHelper
    protected static final int STATE_CLOSE     = 0;
    protected static final int STATE_CLOSING   = 1;
    protected static final int STATE_OPEN      = 2;
    protected static final int STATE_OPENING   = 3;
    protected static final int STATE_DRAGGING  = 4;

    private static final int DEFAULT_MIN_FLING_VELOCITY = 300; // dp per second
    private static final int DEFAULT_MIN_DIST_REQUEST_DISALLOW_PARENT = 1; // dp

    public static final int DRAG_EDGE_LEFT =   0x1;
    public static final int DRAG_EDGE_RIGHT =  0x1 << 1;
    public static final int DRAG_EDGE_TOP =    0x1 << 2;
    public static final int DRAG_EDGE_BOTTOM = 0x1 << 3;

    /**
     * The secondary view will be under the main view.
     */
    public static final int MODE_NORMAL = 0;

    /**
     * The secondary view will stick the edge of the main view.
     */
    public static final int MODE_SAME_LEVEL = 1;

    /**
     * Main view is the view which is shown when the layout is closed.
     */
    private Component mMainView;

    /**
     * Secondary view is the view which is shown when the layout is opened.
     */
    private Component mSecondaryView;

    /**
     * The rectangle position of the main view when the layout is closed.
     */
    private Rect mRectMainClose = new Rect();

    /**
     * The rectangle position of the main view when the layout is opened.
     */
    private Rect mRectMainOpen  = new Rect();

    /**
     * The rectangle position of the secondary view when the layout is closed.
     */
    private Rect mRectSecClose  = new Rect();

    /**
     * The rectangle position of the secondary view when the layout is opened.
     */
    private Rect mRectSecOpen   = new Rect();

    /**
     * The minimum distance (px) to the closest drag edge that the SwipeRevealLayout
     * will disallow the parent to intercept touch event.
     */
    private int mMinDistRequestDisallowParent = 0;

    private boolean mIsOpenBeforeInit = false;
    private volatile boolean mAborted = false;
    private volatile boolean mIsScrolling = false;
    private volatile boolean mLockDrag = false;

    private int mMinFlingVelocity = DEFAULT_MIN_FLING_VELOCITY;
    private int mState = STATE_CLOSE;
    private int mMode = MODE_NORMAL;

    private int mLastMainLeft = 0;
    private int mLastMainTop  = 0;

    private int mDragEdge = DRAG_EDGE_LEFT;

    private float mDragDist = 0;
    private float mPrevX = -1;
    private float mPrevY = -1;

    private ViewDragHelper mDragHelper;
    private GestureDetector mGestureDetector;

    private DragStateChangeListener mDragStateChangeListener; // only used for ViewBindHelper
    private SwipeListener mSwipeListener;

    private int mOnLayoutCount = 0;

    interface DragStateChangeListener {
        void onDragStateChanged(int state);
    }

    /**
     * Listener for monitoring events about swipe layout.
     */
    public interface SwipeListener {
        /**
         * Called when the main view becomes completely closed.
         */
        void onClosed(SwipeLayout view);

        /**
         * Called when the main view becomes completely opened.
         */
        void onOpened(SwipeLayout view);

        /**
         * Called when the main view's position changes.
         * @param slideOffset The new offset of the main view within its range, from 0-1
         */
        void onSlide(SwipeLayout view, float slideOffset);
    }

    /**
     * No-op stub for {@link SwipeListener}. If you only want ot implement a subset
     * of the listener methods, you can extend this instead of implement the full interface.
     */
    public static class SimpleSwipeListener implements SwipeListener {
        @Override
        public void onClosed(SwipeLayout view) {}

        @Override
        public void onOpened(SwipeLayout view) {}

        @Override
        public void onSlide(SwipeLayout view, float slideOffset) {}
    }

    public SwipeLayout(Context context) {
        super(context);
        init(context, null);
    }

    public SwipeLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SwipeLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {

        boolean handle = mGestureDetector.onTouchEvent(event);
        mDragHelper.processTouchEvent(event);



        return handle;

        //return true;

    }

    @Override
    public void onRefreshed(Component component) {
        if (mMainView != null) {
            return;
        }
        // get views
        if (getChildCount() >= 2) {
            mSecondaryView = getComponentAt(0);
            mMainView = getComponentAt(1);
        } else if (getChildCount() == 1) {
            mMainView = getComponentAt(0);
        }
    }

    @Override
    public boolean onArrange(int l, int t, int width, int height) {
        int r = l + width;
        int b = t + height;
        onRefreshed(this);
        mAborted = false;

        for (int index = 0; index < getChildCount(); index++) {
            final Component child = getComponentAt(index);

            int left, right, top, bottom;
            left = right = top = bottom = 0;

            final int minLeft = getPaddingLeft();
            final int maxRight = Math.max(r - getPaddingRight() - l, 0);
            final int minTop = getPaddingTop();
            final int maxBottom = Math.max(b - getPaddingBottom() - t, 0);

            int measuredChildHeight = child.getEstimatedHeight();
            int measuredChildWidth = child.getEstimatedWidth();

            // need to take account if child size is match_parent
            final ComponentContainer.LayoutConfig childParams = child.getLayoutConfig();
            boolean matchParentHeight = false;
            boolean matchParentWidth = false;

            if (childParams != null) {
                matchParentHeight = childParams.height == LayoutConfig.MATCH_PARENT;
                matchParentWidth = childParams.width == LayoutConfig.MATCH_PARENT;
            }

            if (matchParentHeight) {
                measuredChildHeight = maxBottom - minTop;
                childParams.height = measuredChildHeight;
            }

            if (matchParentWidth) {
                measuredChildWidth = maxRight - minLeft;
                childParams.width = measuredChildWidth;
            }

            switch (mDragEdge) {
                case DRAG_EDGE_RIGHT:
                    left    = Math.max(r - measuredChildWidth - getPaddingRight() - l, minLeft);
                    top     = Math.min(getPaddingTop(), maxBottom);
                    right   = Math.max(r - getPaddingRight() - l, minLeft);
                    bottom  = Math.min(measuredChildHeight + getPaddingTop(), maxBottom);
                    break;

                case DRAG_EDGE_LEFT:
                    left    = Math.min(getPaddingLeft(), maxRight);
                    top     = Math.min(getPaddingTop(), maxBottom);
                    right   = Math.min(measuredChildWidth + getPaddingLeft(), maxRight);
                    bottom  = Math.min(measuredChildHeight + getPaddingTop(), maxBottom);
                    break;

                case DRAG_EDGE_TOP:
                    left    = Math.min(getPaddingLeft(), maxRight);
                    top     = Math.min(getPaddingTop(), maxBottom);
                    right   = Math.min(measuredChildWidth + getPaddingLeft(), maxRight);
                    bottom  = Math.min(measuredChildHeight + getPaddingTop(), maxBottom);
                    break;

                case DRAG_EDGE_BOTTOM:
                    left    = Math.min(getPaddingLeft(), maxRight);
                    top     = Math.max(b - measuredChildHeight - getPaddingBottom() - t, minTop);
                    right   = Math.min(measuredChildWidth + getPaddingLeft(), maxRight);
                    bottom  = Math.max(b - getPaddingBottom() - t, minTop);
                    break;
            }

            child.arrange(left, top, right - left, bottom - top);
        }

        // taking account offset when mode is SAME_LEVEL
        if (mMode == MODE_SAME_LEVEL) {
            Rect secondaryPosition = mSecondaryView.getComponentPosition();
            switch (mDragEdge) {
                case DRAG_EDGE_LEFT:
                    secondaryPosition.translate(-mSecondaryView.getWidth(), 0);
                    break;

                case DRAG_EDGE_RIGHT:
                    secondaryPosition.translate(mSecondaryView.getWidth(), 0);
                    break;

                case DRAG_EDGE_TOP:
                    secondaryPosition.translate(0, -mSecondaryView.getHeight());
                    break;

                case DRAG_EDGE_BOTTOM:
                    secondaryPosition.translate(0, mSecondaryView.getHeight());
                    break;
            }
            mSecondaryView.setComponentPosition(secondaryPosition);
        }

        initRects();

        if (mIsOpenBeforeInit) {
            open(false);
        } else {
            close(false);
        }

        mLastMainLeft = mMainView.getLeft();
        mLastMainTop = mMainView.getTop();

        mOnLayoutCount++;
        return true;
    }

    @Override
    public void estimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        super.estimateSize(widthEstimatedConfig, heightEstimatedConfig);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        if (getChildCount() < 2) {
            throw new RuntimeException("Layout must have two children");
        }

        final LayoutConfig params = getLayoutConfig();

        final int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        final int heightMode = EstimateSpec.getMode(heightMeasureSpec);

        int desiredWidth = 0;
        int desiredHeight = 0;

        // first find the largest child
        for (int i = 0; i < getChildCount(); i++) {
            final Component child = getComponentAt(i);
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            desiredWidth = Math.max(child.getEstimatedWidth(), desiredWidth);
            desiredHeight = Math.max(child.getEstimatedHeight(), desiredHeight);
        }
        // create new measure spec using the largest child width
        widthMeasureSpec = EstimateSpec.getSizeWithMode(desiredWidth, widthMode);
        heightMeasureSpec = EstimateSpec.getSizeWithMode(desiredHeight, heightMode);

        final int measuredWidth = EstimateSpec.getSize(widthMeasureSpec);
        final int measuredHeight = EstimateSpec.getSize(heightMeasureSpec);

        for (int i = 0; i < getChildCount(); i++) {
            final Component child = getComponentAt(i);
            final LayoutConfig childParams = child.getLayoutConfig();

            if (childParams != null) {
                if (childParams.height == LayoutConfig.MATCH_PARENT) {
                    child.setMinHeight(measuredHeight);
                }

                if (childParams.width == LayoutConfig.MATCH_PARENT) {
                    child.setMinWidth(measuredWidth);
                }
            }

            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            desiredWidth = Math.max(child.getEstimatedWidth(), desiredWidth);
            desiredHeight = Math.max(child.getEstimatedHeight(), desiredHeight);
        }

        // taking accounts of padding
        desiredWidth += getPaddingLeft() + getPaddingRight();
        desiredHeight += getPaddingTop() + getPaddingBottom();

        // adjust desired width
        if (widthMode == EstimateSpec.PRECISE) {
            desiredWidth = measuredWidth;
        } else {
            if (params.width == LayoutConfig.MATCH_PARENT) {
                desiredWidth = measuredWidth;
            }

            if (widthMode == EstimateSpec.NOT_EXCEED) {
                desiredWidth = (desiredWidth > measuredWidth)? measuredWidth : desiredWidth;
            }
        }

        // adjust desired height
        if (heightMode == EstimateSpec.PRECISE) {
            desiredHeight = measuredHeight;
        } else {
            if (params.height == LayoutConfig.MATCH_PARENT) {
                desiredHeight = measuredHeight;
            }

            if (heightMode == EstimateSpec.NOT_EXCEED) {
                desiredHeight = (desiredHeight > measuredHeight)? measuredHeight : desiredHeight;
            }
        }

        setEstimatedSize(desiredWidth, desiredHeight);
        return true;
    }

    private void computeScroll() {
        if (mDragHelper.continueSettling(true)) {
            this.getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }

    /**
     * Open the panel to show the secondary view
     * @param animation true to animate the open motion. {@link SwipeListener} won't be
     *                  called if is animation is false.
     */
    public void open(boolean animation) {
        mIsOpenBeforeInit = true;
        // bugfix
        if (mMainView == null) {
            return;
        }
        mAborted = false;

        if (animation) {
            mState = STATE_OPENING;
            mDragHelper.smoothSlideViewTo(mMainView, mRectMainOpen.left, mRectMainOpen.top);

            if (mDragStateChangeListener != null) {
                mDragStateChangeListener.onDragStateChanged(mState);
            }
        } else {
            mState = STATE_OPEN;
            mDragHelper.abort();

            mMainView.arrange(
                    mRectMainOpen.left,
                    mRectMainOpen.top,
                    mRectMainOpen.getWidth(),
                    mRectMainOpen.getHeight()
            );

            mSecondaryView.arrange(
                    mRectSecOpen.left,
                    mRectSecOpen.top,
                    mRectSecOpen.getWidth(),
                    mRectSecOpen.getHeight()
            );
        }

        SwipeLayout.this.invalidate();
    }

    /**
     * Close the panel to hide the secondary view
     * @param animation true to animate the close motion. {@link SwipeListener} won't be
     *                  called if is animation is false.
     */
    public void close(boolean animation) {
        // bugfix
        if (mMainView == null) {
            //还未加到布局树
            return;
        }
        mIsOpenBeforeInit = false;
        mAborted = false;

        if (animation) {
            mState = STATE_CLOSING;
            mDragHelper.smoothSlideViewTo(mMainView, mRectMainClose.left, mRectMainClose.top);

            if (mDragStateChangeListener != null) {
                mDragStateChangeListener.onDragStateChanged(mState);
            }

        } else {
            mState = STATE_CLOSE;
            mDragHelper.abort();

            mMainView.arrange(
                    mRectMainClose.left,
                    mRectMainClose.top,
                    mRectMainClose.getWidth(),
                    mRectMainClose.getHeight()
            );

            mSecondaryView.arrange(
                    mRectSecClose.left,
                    mRectSecClose.top,
                    mRectSecClose.getWidth(),
                    mRectSecClose.getHeight()
            );
        }

        SwipeLayout.this.invalidate();
    }

    /**
     * Set the minimum fling velocity to cause the layout to open/close.
     * @param velocity dp per second
     */
    public void setMinFlingVelocity(int velocity) {
        mMinFlingVelocity = velocity;
    }

    /**
     * Get the minimum fling velocity to cause the layout to open/close.
     * @return dp per second
     */
    public int getMinFlingVelocity() {
        return mMinFlingVelocity;
    }

    /**
     * Set the edge where the layout can be dragged from.
     * @param dragEdge Can be one of these
     *                 <ul>
     *                      <li>{@link #DRAG_EDGE_LEFT}</li>
     *                      <li>{@link #DRAG_EDGE_TOP}</li>
     *                      <li>{@link #DRAG_EDGE_RIGHT}</li>
     *                      <li>{@link #DRAG_EDGE_BOTTOM}</li>
     *                 </ul>
     */
    public void setDragEdge(int dragEdge) {
        mDragEdge = dragEdge;
    }

    /**
     * Get the edge where the layout can be dragged from.
     * @return Can be one of these
     *                 <ul>
     *                      <li>{@link #DRAG_EDGE_LEFT}</li>
     *                      <li>{@link #DRAG_EDGE_TOP}</li>
     *                      <li>{@link #DRAG_EDGE_RIGHT}</li>
     *                      <li>{@link #DRAG_EDGE_BOTTOM}</li>
     *                 </ul>
     */
    public int getDragEdge() {
        return mDragEdge;
    }

    public void setSwipeListener(SwipeListener listener) {
        mSwipeListener = listener;
    }

    /**
     * @param lock if set to true, the user cannot drag/swipe the layout.
     */
    public void setLockDrag(boolean lock) {
        mLockDrag = lock;
    }

    /**
     * @return true if the drag/swipe motion is currently locked.
     */
    public boolean isDragLocked() {
        return mLockDrag;
    }


    public boolean isOpened() {
        return (mState == STATE_OPEN);
    }


    public boolean isClosed() {
        return (mState == STATE_CLOSE);
    }


    void setDragStateChangeListener(DragStateChangeListener listener) {
        mDragStateChangeListener = listener;
    }


    protected void abort() {
        mAborted = true;
        mDragHelper.abort();
    }


    protected boolean shouldRequestLayout() {
        return mOnLayoutCount < 2;
    }


    private int getMainOpenLeft() {
        switch (mDragEdge) {
            case DRAG_EDGE_LEFT:
                return mRectMainClose.left + mSecondaryView.getWidth();

            case DRAG_EDGE_RIGHT:
                return mRectMainClose.left - mSecondaryView.getWidth();

            case DRAG_EDGE_TOP:
                return mRectMainClose.left;

            case DRAG_EDGE_BOTTOM:
                return mRectMainClose.left;

            default:
                return 0;
        }
    }

    private int getMainOpenTop() {
        switch (mDragEdge) {
            case DRAG_EDGE_LEFT:
                return mRectMainClose.top;

            case DRAG_EDGE_RIGHT:
                return mRectMainClose.top;

            case DRAG_EDGE_TOP:
                return mRectMainClose.top + mSecondaryView.getHeight();

            case DRAG_EDGE_BOTTOM:
                return mRectMainClose.top - mSecondaryView.getHeight();

            default:
                return 0;
        }
    }

    private int getSecOpenLeft() {
        if (mMode == MODE_NORMAL || mDragEdge == DRAG_EDGE_BOTTOM || mDragEdge == DRAG_EDGE_TOP) {
            return mRectSecClose.left;
        }

        if (mDragEdge == DRAG_EDGE_LEFT) {
            return mRectSecClose.left + mSecondaryView.getWidth();
        } else {
            return mRectSecClose.left - mSecondaryView.getWidth();
        }
    }

    private int getSecOpenTop() {
        if (mMode == MODE_NORMAL || mDragEdge == DRAG_EDGE_LEFT || mDragEdge == DRAG_EDGE_RIGHT) {
            return mRectSecClose.top;
        }

        if (mDragEdge == DRAG_EDGE_TOP) {
            return mRectSecClose.top + mSecondaryView.getHeight();
        } else {
            return mRectSecClose.top - mSecondaryView.getHeight();
        }
    }

    private void initRects() {
        // close position of main view
        mRectMainClose.set(
                mMainView.getLeft(),
                mMainView.getTop(),
                mMainView.getRight(),
                mMainView.getBottom()
        );

        // close position of secondary view
        mRectSecClose.set(
                mSecondaryView.getLeft(),
                mSecondaryView.getTop(),
                mSecondaryView.getRight(),
                mSecondaryView.getBottom()
        );

        // open position of the main view
        mRectMainOpen.set(
                getMainOpenLeft(),
                getMainOpenTop(),
                getMainOpenLeft() + mMainView.getWidth(),
                getMainOpenTop() + mMainView.getHeight()
        );

        // open position of the secondary view
        mRectSecOpen.set(
                getSecOpenLeft(),
                getSecOpenTop(),
                getSecOpenLeft() + mSecondaryView.getWidth(),
                getSecOpenTop() + mSecondaryView.getHeight()
        );
    }

    private boolean couldBecomeClick(TouchEvent ev) {
        return isInMainView(ev) && !shouldInitiateADrag();
    }

    private boolean isInMainView(TouchEvent ev) {
        float x = ev.getPointerPosition(ev.getIndex()).getX();
        float y = ev.getPointerPosition(ev.getIndex()).getY();
        boolean withinVertical = mMainView.getTop() <= y && y <= mMainView.getBottom();
        boolean withinHorizontal = mMainView.getLeft() <= x && x <= mMainView.getRight();

        return withinVertical && withinHorizontal;
    }

    private boolean shouldInitiateADrag() {
        float minDistToInitiateDrag = mDragHelper.getTouchSlop();
        return mDragDist >= minDistToInitiateDrag;
    }

    private void accumulateDragDist(TouchEvent ev) {
        final int action = ev.getAction();
        if (action == TouchEvent.PRIMARY_POINT_DOWN) {
            mDragDist = 0;
            return;
        }

        boolean dragHorizontally = getDragEdge() == DRAG_EDGE_LEFT
                || getDragEdge() == DRAG_EDGE_RIGHT;

        float dragged;
        if (dragHorizontally) {
            dragged = Math.abs(ev.getPointerPosition(ev.getIndex()).getX() - mPrevX);
        } else {
            dragged = Math.abs(ev.getPointerPosition(ev.getIndex()).getY() - mPrevY);
        }

        mDragDist += dragged;
    }

    private void init(Context context, AttrSet attrs) {
        if (attrs != null && context != null) {
            mDragEdge = parseDragEdge(ResourceUtil.getString(attrs, "dragEdge", "left"));
            mMinFlingVelocity = ResourceUtil.getInteger(attrs, "flingVelocity", DEFAULT_MIN_FLING_VELOCITY);
            mMode = parseMode(ResourceUtil.getString(attrs, "mode", "normal"));
            mMinDistRequestDisallowParent = ResourceUtil.getDimension(attrs, "minDistRequestDisallowParent",
                    dpToPx(DEFAULT_MIN_DIST_REQUEST_DISALLOW_PARENT)
            );
        }

        mDragHelper = ViewDragHelper.create(this, 1.0f, mDragHelperCallback);
        mDragHelper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_ALL);

        mGestureDetector = new GestureDetector(context, mGestureListener);
        setTouchEventListener(this);
        setLayoutRefreshedListener(this);
        setEstimateSizeListener(this);
        setArrangeListener(this);
        addDrawTask(new DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                computeScroll();
            }
        });

        if (getChildCount() >= 2) {
            mSecondaryView = getComponentAt(0);
            mMainView = getComponentAt(1);
        } else if (getChildCount() == 1) {
            mMainView = getComponentAt(0);
        }
    }

    private int parseDragEdge(String dragEdge) {
        switch (dragEdge) {
            case "left":
                return DRAG_EDGE_LEFT;
            case "right":
                return DRAG_EDGE_RIGHT;
            case "top":
                return DRAG_EDGE_TOP;
            case "bottom":
                return DRAG_EDGE_BOTTOM;
            default:
                return DRAG_EDGE_LEFT;
        }
    }

    private int parseMode(String mode) {
        switch (mode) {
            case "normal":
                return MODE_NORMAL;
            case "same_level":
                return MODE_SAME_LEVEL;
            default:
                return MODE_NORMAL;
        }
    }

    private final GestureDetector.OnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {
        boolean hasDisallowed = false;

        @Override
        public boolean onDown(TouchEvent e) {
            mIsScrolling = false;
            hasDisallowed = false;
            return true;
        }

        @Override
        public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
            mIsScrolling = true;
            return false;
        }

        @Override
        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
            mIsScrolling = true;

            if (getComponentParent() != null) {
                boolean shouldDisallow;

                if (!hasDisallowed) {
                    shouldDisallow = getDistToClosestEdge() >= mMinDistRequestDisallowParent;
                    if (shouldDisallow) {
                        hasDisallowed = true;
                    }
                } else {
                    shouldDisallow = true;
                }

                // disallow parent to intercept touch event so that the layout will work
                // properly on RecyclerView or view that handles scroll gesture.
                //getComponentParent().requestDisallowInterceptTouchEvent(shouldDisallow);
            }

            return false;
        }
    };

    private int getDistToClosestEdge() {
        switch (mDragEdge) {
            case DRAG_EDGE_LEFT:
                final int pivotRight = mRectMainClose.left + mSecondaryView.getWidth();

                return Math.min(
                        mMainView.getLeft() - mRectMainClose.left,
                        pivotRight - mMainView.getLeft()
                );

            case DRAG_EDGE_RIGHT:
                final int pivotLeft = mRectMainClose.right - mSecondaryView.getWidth();

                return Math.min(
                        mMainView.getRight() - pivotLeft,
                        mRectMainClose.right - mMainView.getRight()
                );

            case DRAG_EDGE_TOP:
                final int pivotBottom = mRectMainClose.top + mSecondaryView.getHeight();

                return Math.min(
                        mMainView.getBottom() - pivotBottom,
                        pivotBottom - mMainView.getTop()
                );

            case DRAG_EDGE_BOTTOM:
                final int pivotTop = mRectMainClose.bottom - mSecondaryView.getHeight();

                return Math.min(
                        mRectMainClose.bottom - mMainView.getBottom(),
                        mMainView.getBottom() - pivotTop
                );
        }

        return 0;
    }

    private int getHalfwayPivotHorizontal() {
        if (mDragEdge == DRAG_EDGE_LEFT) {
            return mRectMainClose.left + mSecondaryView.getWidth() / 2;
        } else {
            return mRectMainClose.right - mSecondaryView.getWidth() / 2;
        }
    }

    private int getHalfwayPivotVertical() {
        if (mDragEdge == DRAG_EDGE_TOP) {
            return mRectMainClose.top + mSecondaryView.getHeight() / 2;
        } else {
            return mRectMainClose.bottom - mSecondaryView.getHeight() / 2;
        }
    }

    private final ViewDragHelper.Callback mDragHelperCallback = new ViewDragHelper.Callback() {
        @Override
        public boolean tryCaptureView(Component child, int pointerId) {
            mAborted = false;

            if (mLockDrag)
                return false;

            mDragHelper.captureChildView(mMainView, pointerId);
            return false;
        }

        @Override
        public int clampViewPositionVertical(Component child, int top, int dy) {
            switch (mDragEdge) {
                case DRAG_EDGE_TOP:
                    return Math.max(
                            Math.min(top, mRectMainClose.top + mSecondaryView.getHeight()),
                            mRectMainClose.top
                    );

                case DRAG_EDGE_BOTTOM:
                    return Math.max(
                            Math.min(top, mRectMainClose.top),
                            mRectMainClose.top - mSecondaryView.getHeight()
                    );

                default:
                    return child.getTop();
            }
        }

        @Override
        public int clampViewPositionHorizontal(Component child, int left, int dx) {
            switch (mDragEdge) {
                case DRAG_EDGE_RIGHT:
                    return Math.max(
                            Math.min(left, mRectMainClose.left),
                            mRectMainClose.left - mSecondaryView.getWidth()
                    );

                case DRAG_EDGE_LEFT:
                    return Math.max(
                            Math.min(left, mRectMainClose.left + mSecondaryView.getWidth()),
                            mRectMainClose.left
                    );

                default:
                    return (int) child.getTranslationX();
            }
        }

        @Override
        public void onViewReleased(Component releasedChild, float xvel, float yvel) {
            final boolean velRightExceeded =  pxToDp((int) xvel) >= mMinFlingVelocity;
            final boolean velLeftExceeded =   pxToDp((int) xvel) <= -mMinFlingVelocity;
            final boolean velUpExceeded =     pxToDp((int) yvel) <= -mMinFlingVelocity;
            final boolean velDownExceeded =   pxToDp((int) yvel) >= mMinFlingVelocity;

            final int pivotHorizontal = getHalfwayPivotHorizontal();
            final int pivotVertical = getHalfwayPivotVertical();

            switch (mDragEdge) {
                case DRAG_EDGE_RIGHT:
                    if (velRightExceeded) {
                        close(true);
                    } else if (velLeftExceeded) {
                        open(true);
                    } else {
                        if (mMainView.getRight() < pivotHorizontal) {
                            open(true);
                        } else {
                            close(true);
                        }
                    }
                    break;

                case DRAG_EDGE_LEFT:
                    if (velRightExceeded) {
                        open(true);
                    } else if (velLeftExceeded) {
                        close(true);
                    } else {
                        if (mMainView.getLeft() < pivotHorizontal) {
                            close(true);
                        } else {
                            open(true);
                        }
                    }
                    break;

                case DRAG_EDGE_TOP:
                    if (velUpExceeded) {
                        close(true);
                    } else if (velDownExceeded) {
                        open(true);
                    } else {
                        if (mMainView.getTop() < pivotVertical) {
                            close(true);
                        } else {
                            open(true);
                        }
                    }
                    break;

                case DRAG_EDGE_BOTTOM:
                    if (velUpExceeded) {
                        open(true);
                    } else if (velDownExceeded) {
                        close(true);
                    } else {
                        if (mMainView.getBottom() < pivotVertical) {
                            open(true);
                        } else {
                            close(true);
                        }
                    }
                    break;
            }
        }

        @Override
        public void onEdgeDragStarted(int edgeFlags, int pointerId) {
            super.onEdgeDragStarted(edgeFlags, pointerId);

            if (mLockDrag) {
                return;
            }

            boolean edgeStartLeft = (mDragEdge == DRAG_EDGE_RIGHT)
                    && edgeFlags == ViewDragHelper.EDGE_LEFT;

            boolean edgeStartRight = (mDragEdge == DRAG_EDGE_LEFT)
                    && edgeFlags == ViewDragHelper.EDGE_RIGHT;

            boolean edgeStartTop = (mDragEdge == DRAG_EDGE_BOTTOM)
                    && edgeFlags == ViewDragHelper.EDGE_TOP;

            boolean edgeStartBottom = (mDragEdge == DRAG_EDGE_TOP)
                    && edgeFlags == ViewDragHelper.EDGE_BOTTOM;

            if (edgeStartLeft || edgeStartRight || edgeStartTop || edgeStartBottom) {
                mDragHelper.captureChildView(mMainView, pointerId);
            }
        }

        @Override
        public void onViewPositionChanged(Component changedView, int left, int top, int dx, int dy) {
            super.onViewPositionChanged(changedView, left, top, dx, dy);
            Rect secondaryPosition = mSecondaryView.getComponentPosition();
            if (mMode == MODE_SAME_LEVEL) {
                if (mDragEdge == DRAG_EDGE_LEFT || mDragEdge == DRAG_EDGE_RIGHT) {
                    secondaryPosition.translate(dx, 0);
                } else {
                    secondaryPosition.translate(0, dy);
                }
                mSecondaryView.setComponentPosition(secondaryPosition);
            }

            boolean isMoved = (mMainView.getLeft() != mLastMainLeft) || (mMainView.getTop() != mLastMainTop);
            if (mSwipeListener != null && isMoved) {
                if (mMainView.getLeft() == mRectMainClose.left && mMainView.getTop() == mRectMainClose.top) {
                    mSwipeListener.onClosed(SwipeLayout.this);
                }
                else if (mMainView.getLeft() == mRectMainOpen.left && mMainView.getTop() == mRectMainOpen.top) {
                    mSwipeListener.onOpened(SwipeLayout.this);
                }
                else {
                    mSwipeListener.onSlide(SwipeLayout.this, getSlideOffset());
                }
            }

            mLastMainLeft = mMainView.getLeft();
            mLastMainTop = mMainView.getTop();
            SwipeLayout.this.invalidate();
        }

        private float getSlideOffset() {
            switch (mDragEdge) {
                case DRAG_EDGE_LEFT:
                    return (float) (mMainView.getLeft() - mRectMainClose.left) / mSecondaryView.getWidth();

                case DRAG_EDGE_RIGHT:
                    return (float) (mRectMainClose.left - mMainView.getLeft()) / mSecondaryView.getWidth();

                case DRAG_EDGE_TOP:
                    return (float) (mMainView.getTop() - mRectMainClose.top) / mSecondaryView.getHeight();

                case DRAG_EDGE_BOTTOM:
                    return (float) (mRectMainClose.top - mMainView.getTop()) / mSecondaryView.getHeight();

                default:
                    return 0;
            }
        }

        @Override
        public void onViewDragStateChanged(int state) {
            super.onViewDragStateChanged(state);
            final int prevState = mState;

            switch (state) {
                case ViewDragHelper.STATE_DRAGGING:
                    mState = STATE_DRAGGING;
                    break;

                case ViewDragHelper.STATE_IDLE:

                    // drag edge is left or right
                    if (mDragEdge == DRAG_EDGE_LEFT || mDragEdge == DRAG_EDGE_RIGHT) {
                        if (mMainView.getLeft() == mRectMainClose.left) {
                            mState = STATE_CLOSE;
                        } else {
                            mState = STATE_OPEN;
                        }
                    }

                    // drag edge is top or bottom
                    else {
                        if (mMainView.getTop() == mRectMainClose.top) {
                            mState = STATE_CLOSE;
                        } else {
                            mState = STATE_OPEN;
                        }
                    }
                    break;
            }

            if (mDragStateChangeListener != null && !mAborted && prevState != mState) {
                mDragStateChangeListener.onDragStateChanged(mState);
            }
        }
    };

    public static String getStateString(int state) {
        switch (state) {
            case STATE_CLOSE:
                return "state_close";

            case STATE_CLOSING:
                return "state_closing";

            case STATE_OPEN:
                return "state_open";

            case STATE_OPENING:
                return "state_opening";

            case STATE_DRAGGING:
                return "state_dragging";

            default:
                return "undefined";
        }
    }

    private int pxToDp(int px) {
        float scale =
                DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().scalDensity;
        return (int) (px / scale + 0.5f);
    }

    private int dpToPx(int dp) {
        float scale =
                DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().scalDensity;
        return (int) (dp * scale + 0.5f);
    }

    private void measureChild(Component child, int parentWidthMeasureSpec,
                              int parentHeightMeasureSpec) {
        final LayoutConfig lp = child.getLayoutConfig();

        final int childWidthMeasureSpec = getChildMeasureSpec(parentWidthMeasureSpec,
                getPaddingLeft() + getPaddingRight(), lp.width);
        final int childHeightMeasureSpec = getChildMeasureSpec(parentHeightMeasureSpec,
                getPaddingTop() + getPaddingBottom(), lp.height);

        child.estimateSize(childWidthMeasureSpec, childHeightMeasureSpec);
    }

    /**
     * Does the hard part of measureChildren: figuring out the MeasureSpec to
     * pass to a particular child. This method figures out the right MeasureSpec
     * for one dimension (height or width) of one child view.
     * <p>
     * The goal is to combine information from our MeasureSpec with the
     * LayoutParams of the child to get the best possible results. For example,
     * if the this view knows its size (because its MeasureSpec has a mode of
     * PRECISE), and the child has indicated in its LayoutParams that it wants
     * to be the same size as the parent, the parent should ask the child to
     * layout given an exact size.
     *
     * @param spec           The requirements for this view
     * @param padding        The padding of this view for the current dimension and
     *                       margins, if applicable
     * @param childDimension How big the child wants to be in the current
     *                       dimension
     * @return a MeasureSpec integer for the child
     */
    private static int getChildMeasureSpec(int spec, int padding, int childDimension) {
        int specMode = EstimateSpec.getMode(spec);
        int specSize = EstimateSpec.getSize(spec);
        int size = Math.max(0, specSize - padding);

        int resultSize = 0;
        int resultMode = 0;

        switch (specMode) {
            // Parent has imposed an exact size on us
            case EstimateSpec.PRECISE:
                if (childDimension >= 0) {
                    resultSize = childDimension;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_PARENT) {
                    // Child wants to be our size. So be it.
                    resultSize = size;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_CONTENT) {
                    // Child wants to determine its own size. It can't be
                    // bigger than us.
                    resultSize = size;
                    resultMode = EstimateSpec.NOT_EXCEED;
                }
                break;

            // Parent has imposed a maximum size on us
            case EstimateSpec.NOT_EXCEED:
                if (childDimension >= 0) {
                    // Child wants a specific size... so be it
                    resultSize = childDimension;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_PARENT) {
                    // Child wants to be our size, but our size is not fixed.
                    // Constrain child to not be bigger than us.
                    resultSize = size;
                    resultMode = EstimateSpec.NOT_EXCEED;
                } else if (childDimension == LayoutConfig.MATCH_CONTENT) {
                    // Child wants to determine its own size. It can't be
                    // bigger than us.
                    resultSize = size;
                    resultMode = EstimateSpec.NOT_EXCEED;
                }
                break;

            // Parent asked to see how big we want to be
            case EstimateSpec.UNCONSTRAINT:
                if (childDimension >= 0) {
                    // Child wants a specific size... let him have it
                    resultSize = childDimension;
                    resultMode = EstimateSpec.PRECISE;
                } else if (childDimension == LayoutConfig.MATCH_PARENT) {
                    // Child wants to be our size... find out how big it should
                    // be
                    resultSize = size;
                    resultMode = EstimateSpec.UNCONSTRAINT;
                } else if (childDimension == LayoutConfig.MATCH_CONTENT) {
                    // Child wants to determine its own size.... find out how
                    // big it should be
                    resultSize = size;
                    resultMode = EstimateSpec.UNCONSTRAINT;
                }
                break;
            default:
                break;
        }
        //noinspection ResourceType
        return EstimateSpec.getSizeWithMode(resultSize, resultMode);
    }
}
