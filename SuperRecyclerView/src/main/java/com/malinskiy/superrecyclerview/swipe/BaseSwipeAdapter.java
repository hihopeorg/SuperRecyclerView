package com.malinskiy.superrecyclerview.swipe;

import com.malinskiy.superrecyclerview.ResourceTable;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;



import java.util.List;


public class BaseSwipeAdapter extends BaseItemProvider {
    private final LayoutScatter mInflater;
    private final ViewBinderHelper binderHelper;
    private List<String> objects;
    private Context context;

    public BaseSwipeAdapter(Context context, List<String> objects) {
        super();
        this.context = context;
        this.objects = objects;
        mInflater = LayoutScatter.getInstance(context);
        binderHelper = new ViewBinderHelper();

        // uncomment if you want to open only one row at a time
        // binderHelper.setOpenOnlyOne(true);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.parse(ResourceTable.Layout_row_list, parent, false);

            holder = new ViewHolder();
            holder.swipeLayout = (SwipeLayout) convertView.findComponentById(ResourceTable.Id_swipe_layout);
            holder.frontView = convertView.findComponentById(ResourceTable.Id_front_layout);
            holder.deleteView = convertView.findComponentById(ResourceTable.Id_delete_layout);
            holder.textView = (Text) convertView.findComponentById(ResourceTable.Id_text);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final String item = (String) getItem(position);
        if (item != null) {
            binderHelper.bind(holder.swipeLayout, item);

            holder.textView.setText(item);
            holder.deleteView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    objects.remove(item);
                    BaseSwipeAdapter.this.notifyDataChanged();
                }
            });

        }

        return convertView;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int i) {
        return objects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void add(String s) {
        objects.add(s);
        notifyDataChanged();
    }


    private class ViewHolder {
        SwipeLayout swipeLayout;
        Component frontView;
        Component deleteView;
        Text textView;
    }
}
