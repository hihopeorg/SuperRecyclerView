package com.malinskiy.superrecyclerview.refresh;

import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;


public abstract class HeadOverComponent extends StackLayout {

    public enum RefreshState {
        /**
         * 初始态
         */
        STATE_INIT,
        /**
         * 下拉刷新的头部可见
         */
        STATE_VISIBLE,
        /**
         * 正在刷新的状态
         */
        STATE_REFRESH,
        /**
         * 超出可刷新距离的状态
         */
        STATE_OVER,
        /**
         * 超出刷新位置松开手后的状态
         */
        STATE_OVER_RELEASE
    }

    protected RefreshState mState = RefreshState.STATE_INIT;
    /**
     * 触发下拉刷新时的最小高度，当刚好下拉到这个距离，那就直接刷新，
     * 如果下拉的距离超过了这个距离，那就先滚动到这个距离，然后才开始刷新
     */
    public int mPullRefreshHeight;
    /**
     * 最小阻尼，用户越往下拉，越不跟手
     */
    public float minDamp = 1.6f;
    /**
     * 最大阻尼
     */
    public float maxDamp = 2.2f;

    public HeadOverComponent(Context context) {
        this(context, null);
    }

    public HeadOverComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public HeadOverComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPullRefreshHeight = vp2px(context, 80);
        init();
    }

    /**
     * 初始化
     */
    public abstract void init();

    /**
     * 滚动
     *
     * @param scrollY 纵轴滚动的距离
     * @param pullRefreshHeight 触发下拉刷新时的最小高度
     */
    public abstract void onScroll(int scrollY, int pullRefreshHeight);

    /**
     * 显示头部组件
     */
    public abstract void onVisible();

    /**
     * 超出头部组件高度，松手开始加载
     */
    public abstract void onOver();

    /**
     * 开始刷新
     */
    public abstract void onRefresh();

    /**
     * 刷新完成
     */
    public abstract void onFinish();

    public void setState(RefreshState state) {
        this.mState = state;
    }

    public RefreshState getState() {
        return mState;
    }

    public static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}
