package com.malinskiy.superrecyclerview.refresh;



import com.malinskiy.superrecyclerview.ResourceTable;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;


public class TextOverComponent extends HeadOverComponent {

    private Image mImage;
    private Text mText;
    private AnimatorProperty mAnimator;

    public TextOverComponent(Context context) {
        this(context, null);
    }

    public TextOverComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public TextOverComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    public void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_refresh_over_view, this, true);
        mImage = (Image) findComponentById(ResourceTable.Id_image);
        mText = (Text) findComponentById(ResourceTable.Id_text);
    }

    /**
     * 滚动
     *
     * @param scrollY 纵轴滚动的距离
     * @param pullRefreshHeight 触发下拉刷新时的最小高度
     */
    @Override
    public void onScroll(int scrollY, int pullRefreshHeight) {

    }

    /**
     * 显示头部组件
     */
    @Override
    public void onVisible() {
        mText.setText("下拉刷新");
    }

    /**
     * 超出头部视图高度，松手开始加载
     */
    @Override
    public void onOver() {
        mText.setText("松开刷新");
    }

    /**
     * 正在刷新
     */
    @Override
    public void onRefresh() {
        mText.setText("正在刷新...");
        mAnimator = mImage.createAnimatorProperty();
        mAnimator.setDuration(700).rotate(360).setLoopedCount(AnimatorProperty.INFINITE).setTarget(mImage).start();
    }

    /**
     * 刷新完成
     */
    @Override
    public void onFinish() {
        mAnimator.cancel();
    }
}
