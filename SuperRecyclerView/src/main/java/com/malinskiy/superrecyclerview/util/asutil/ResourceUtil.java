

package com.malinskiy.superrecyclerview.util.asutil;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

public class ResourceUtil {

    public static boolean getBool(AttrSet attrSet, String name, boolean defaultValue) {
        boolean result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getBoolValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static int getInteger(AttrSet attrSet, String name, int defaultValue) {
        int result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getIntegerValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static long getLong(AttrSet attrSet, String name, long defaultValue) {
        long result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getLongValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static float getFloat(AttrSet attrSet, String name, float defaultValue) {
        float result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getFloatValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static String getString(AttrSet attrSet, String name, String defaultValue) {
        String result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getStringValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static int getDimension(AttrSet attrSet, String name, int defaultValue) {
        int result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getDimensionValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Color getColor(AttrSet attrSet, String name, Color defaultValue) {
        Color result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getColorValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Element getElement(AttrSet attrSet, String name, Element defaultValue) {
        Element result = defaultValue;
        if (attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getElement();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
