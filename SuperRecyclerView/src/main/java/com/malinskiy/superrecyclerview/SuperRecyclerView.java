package com.malinskiy.superrecyclerview;


import com.malinskiy.superrecyclerview.refresh.HeadOverComponent;
import com.malinskiy.superrecyclerview.refresh.IRefresh;
import com.malinskiy.superrecyclerview.refresh.SwipeRefreshLayout;
import com.malinskiy.superrecyclerview.refresh.TextOverComponent;

import ohos.agp.components.AttrSet;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutManager;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.database.DataSetSubscriber;
import ohos.app.Context;


public class SuperRecyclerView extends DependentLayout {

    protected int ITEM_LEFT_TO_LOAD_MORE = 10;

    protected ListContainer mRecycler;
    protected Component mMoreProgress;
    protected SwipeRefreshLayout mPtrLayout;

    protected int mSuperRecyclerViewMainLayout;

    protected ListContainer.ScrollListener mInternalOnScrollListener;
    private ListContainer.ScrolledListener mInternalScrollListener;
    private ListContainer.ScrolledListener mSwipeDismissScrollListener;
    protected ListContainer.ScrolledListener mExternalOnScrollListener;

    protected boolean isLoadingMore;

    protected OnMoreListener mOnMoreListener;



    public SuperRecyclerView(Context context) {
        super(context,null);
        initView();
    }

    public SuperRecyclerView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView();
    }

    public SuperRecyclerView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView();
    }

    private void initView() {
        mSuperRecyclerViewMainLayout = ResourceTable.Layout_superrecyclerview;
        Component v = LayoutScatter.getInstance(getContext()).parse(mSuperRecyclerViewMainLayout, this,this!=null);
        mPtrLayout = (SwipeRefreshLayout) v.findComponentById(ResourceTable.Id_ptr_layout);
        mPtrLayout.setEnabled(false);
        mPtrLayout.setRefreshOverComponent(new TextOverComponent(getContext()));

        mMoreProgress = (Component) v.findComponentById(ResourceTable.Id_more_progress);
        mMoreProgress.setVisibility(Component.HIDE);
        initRecyclerView(v);
    }

    private void initRecyclerView(Component v) {
        Component recyclerView = v.findComponentById(ResourceTable.Id_list);

        if (recyclerView instanceof ListContainer)
            mRecycler = (ListContainer) recyclerView;
        else
            throw new IllegalArgumentException("SuperRecyclerView works with a RecyclerView!");



        mInternalOnScrollListener = () -> {

            processOnMore();
        };






        mRecycler.setScrollListener(mInternalOnScrollListener);

    }

    private void processOnMore() {

        int lastVisibleItemPosition = mRecycler.getItemPosByVisibleIndex(mRecycler.getVisibleIndexCount()-1);
        int visibleItemCount = mRecycler.getVisibleIndexCount();
        int totalItemCount = mRecycler.getChildCount();


        if (((totalItemCount - lastVisibleItemPosition) <= ITEM_LEFT_TO_LOAD_MORE ||
                (totalItemCount - lastVisibleItemPosition) == 0 && totalItemCount > visibleItemCount)
                && !isLoadingMore) {

            isLoadingMore = true;
            if (mOnMoreListener != null) {
                mMoreProgress.setVisibility(Component.VISIBLE);
                mOnMoreListener.onMoreAsked(mRecycler.getChildCount(), ITEM_LEFT_TO_LOAD_MORE, lastVisibleItemPosition);
            }
        }

//        if(totalItemCount - lastVisibleItemPosition <= 1){
//            isLoadingMore = true;
//            if (mOnMoreListener != null) {
//                mMoreProgress.setVisibility(Component.VISIBLE);
//                mOnMoreListener.onMoreAsked(mRecycler.getChildCount(), ITEM_LEFT_TO_LOAD_MORE, lastVisibleItemPosition);
//            }
//        }



    }

    private void setAdapterInternal(BaseItemProvider adapter, boolean compatibleWithPrevious,
                                    boolean removeAndRecycleExistingViews) {
        mRecycler.setItemProvider(adapter);
        mRecycler.setVisibility(Component.VISIBLE);
        if (null != adapter)
            adapter.addDataSubscriber(new DataSetSubscriber() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    update();
                }

                @Override
                public void onInvalidated() {
                    super.onInvalidated();
                    update();
                }

                @Override
                public void onItemChanged(int position) {
                    super.onItemChanged(position);
                    update();
                }

                @Override
                public void onItemInserted(int position) {
                    super.onItemInserted(position);
                    update();
                }

                @Override
                public void onItemRemoved(int position) {
                    super.onItemRemoved(position);
                    update();
                }

                @Override
                public void onItemRangeChanged(int startPos, int countItems) {
                    super.onItemRangeChanged(startPos, countItems);
                    update();
                }

                @Override
                public void onItemRangeInserted(int startPos, int countItems) {
                    super.onItemRangeInserted(startPos, countItems);
                    update();
                }

                @Override
                public void onItemRangeRemoved(int startPos, int countItems) {
                    super.onItemRangeRemoved(startPos, countItems);
                    update();
                }

                private void update() {
                    mMoreProgress.setVisibility(Component.HIDE);
                    isLoadingMore = false;
                }
            });
    }



    public void setLayoutManager(LayoutManager manager) {
        mRecycler.setLayoutManager(manager);
    }


    public void setAdapter(BaseItemProvider adapter) {
        setAdapterInternal(adapter, false, true);
    }

    public void setRefreshListener(IRefresh.RefreshListener listener) {
        mPtrLayout.setEnabled(true);
        mPtrLayout.setRefreshListener(listener);
    }

    public void addOnItemTouchListener(ListContainer.ItemClickedListener listener) {
        mRecycler.setItemClickedListener(listener);
    }

    public void removeOnItemTouchListener(ListContainer.ItemClickedListener listener) {
        mRecycler.setItemClickedListener(null);
    }

    public void setRefreshOverComponent(HeadOverComponent headOverComponent) {
        mPtrLayout.setRefreshOverComponent(headOverComponent);
    }

    public BaseItemProvider getAdapter() {
        return mRecycler.getItemProvider();
    }

    public ListContainer getListContainer() {
        return mRecycler;
    }

    public void refreshFinish(){
        mPtrLayout.refreshFinish();
    }

    public void setupMoreListener(OnMoreListener onMoreListener, int max) {
        mOnMoreListener = onMoreListener;
        ITEM_LEFT_TO_LOAD_MORE = max;
    }


}
